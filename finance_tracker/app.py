#!/usr/bin/env python3

import yfinance as yf
import pandas as pd
from flask import request, render_template, jsonify, Flask

app = Flask(__name__, template_folder='templates')



@app.route('/')
def index():
    return render_template('index.html')


@app.route('/stock_data', methods=['POST'])
def stock_data():
    ticker = request.form['ticker']
    data = yf.Ticker(ticker).history(period='1d')
    data_dict =  {'ticker':ticker,
                  'current_price':data.iloc[-1].Close,
                  'open_price': data.iloc[-1].Open}
    return render_template('ticker_cards.html', 
                           ticker=data_dict.get('ticker'),
                           current_price=data_dict.get('current_price'),
                           open_price=data_dict.get('open_price')
                           )



if __name__ == "__main__":
    app.run(debug=True)
